package main

import (
	"fmt"
)

func Palindrome(n int) string {
	num := n
	sum := 0
	for num > 0 {
		sum = ( sum * 10 ) + ( num % 10)
		num /= 10
 	}
    if sum == n {
		return "is Palindrome"
	}
	return "is not Palindrome"
}

func main() {
     fmt.Print("\n", Palindrome(166661))
}

