package main

import "fmt"

func Fibonacci(n int) int {
    if n <= 1 {
        return n
    }
    return Fibonacci(n-1) + Fibonacci(n-2)
}

func main() {
	for i := 0; i <= 9; i++ {
        fmt.Print(" ", Fibonacci(i))
    }
    fmt.Println("")
}
