package main

import (
	"fmt"
)

func FizzBuzz(n int) string {
	text := ""
    if n % 3 == 0 {
        text = "Fizz"
    }
	
	if n % 5 == 0 {
		text += "Buzz"
	}
    return text
}

func main() {
	for i := 0; i <= 15; i++ {
        fmt.Print(" ", FizzBuzz(i))
    }
}

