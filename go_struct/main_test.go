package main

import (
	"testing"

	"github.com/stretchr/testify/require"
)

const (
	dbDriver = "mysql"
	dbSource = "jamshid:1@/go_struct"
)

var testContacts *contacts

func TestCreateContact(t *testing.T) {
	arg := Contact{
		first_name: "tom",
		last_name:  "dasd",
		phone:      "999",
		email:      "ddas@asd.as",
		position:   "dasdas",
	}

	contact, err := testContacts.Insert(arg)
	require.NoError(t, err)
	require.NotEmpty(t, contact)

	require.Equal(t, arg.first_name, contact.first_name)
	require.Equal(t, arg.last_name, contact.last_name)
	require.Equal(t, arg.email, contact.email)
	require.Equal(t, arg.phone, contact.phone)
	require.Equal(t, arg.position, contact.position)

	require.NotZero(t, contact.id)
}
