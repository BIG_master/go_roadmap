package main

import (
	"database/sql"
	"log"

	_ "github.com/go-sql-driver/mysql"
)

type Contact struct {
	id         int
	first_name string
	last_name  string
	phone      string
	email      string
	position   string
}

type contacts struct {
	db *sql.DB
}

func (c *contacts) dbConn() (db *sql.DB) {
	dbDriver := "mysql"
	dbUser := "jamshid"
	dbPass := "1"
	dbName := "go_struct"
	db, err := sql.Open(dbDriver, dbUser+":"+dbPass+"@/"+dbName)
	if err != nil {
		panic(err.Error())
	}
	return db
}

func (c *contacts) GetAll() {
	log.Println("-----GetAll------")
	db := c.dbConn()
	selDB, err := db.Query("SELECT * FROM contacts ORDER BY id DESC")
	if err != nil {
		panic(err.Error())
	}
	res := []Contact{}
	for selDB.Next() {
		var contact Contact
		err = selDB.Scan(&contact.id, &contact.first_name, &contact.last_name, &contact.email, &contact.phone, &contact.position)
		if err != nil {
			panic(err.Error())
		}
		res = append(res, contact)
		log.Println("GetAll: first_name: " + contact.first_name + " | last_name: " + contact.last_name + " | email: " + contact.email + " | phone: " + contact.phone + " | position: " + contact.position)
	}
	defer db.Close()
}

func (c *contacts) Get(nId int) {
	log.Println("-----Get------")
	db := c.dbConn()
	selDB, err := db.Query("SELECT * FROM contacts WHERE id=?", nId)
	if err != nil {
		panic(err.Error())
	}
	for selDB.Next() {
		var contact Contact
		err = selDB.Scan(&contact.id, &contact.first_name, &contact.last_name, &contact.email, &contact.phone, &contact.position)
		if err != nil {
			panic(err.Error())
		}
		log.Println("Get: first_name: " + contact.first_name + " | last_name: " + contact.last_name + " | email: " + contact.email + " | phone: " + contact.phone + " | position: " + contact.position)
	}
	defer db.Close()
}

func (c *contacts) Insert(arg Contact) (Contact, error) {
	log.Println("-----Insert------")
	db := c.dbConn()
	insForm, err := db.Prepare("INSERT INTO contacts(id, first_name, last_name, phone, email, position) VALUES(?,?,?,?,?,?)")
	if err != nil {
		panic(err.Error())
	}
	insForm.Exec(arg.id, arg.first_name, arg.last_name, arg.email, arg.phone, arg.position)
	log.Println("INSERT: first_name: " + arg.first_name + " | last_name: " + arg.last_name + " | email: " + arg.email + " | phone: " + arg.phone + " | position: " + arg.position)
	defer db.Close()
	return arg, err
}

func (c *contacts) Update(arg Contact) {
	log.Println("-----Update------")
	db := c.dbConn()
	insForm, err := db.Prepare("UPDATE contacts SET first_name=?, last_name=?, phone=?, email=?, position=? WHERE id=?")
	if err != nil {
		panic(err.Error())
	}
	insForm.Exec(arg.first_name, arg.last_name, arg.email, arg.phone, arg.position, arg.id)
	log.Println("Update: first_name: " + arg.first_name + " | last_name: " + arg.last_name + " | email: " + arg.email + " | phone: " + arg.phone + " | position: " + arg.position)
	defer db.Close()
}

func (c *contacts) Delete(contact int) {
	log.Println("-----Delete------")
	db := c.dbConn()
	delForm, err := db.Prepare("DELETE FROM contacts WHERE id=?")
	if err != nil {
		panic(err.Error())
	}
	delForm.Exec(contact)
	log.Println("DELETE", contact)
	defer db.Close()
	c.GetAll()
}

func main() {
	var c contacts
	log.Println("c", c)
	contact := Contact{
		first_name: "tom",
		last_name:  "dasd",
		phone:      "999",
		email:      "ddas@asd.as",
		position:   "dasdas",
	}
	c.Insert(contact)
	c.GetAll()
	contact = Contact{
		id:         6,
		first_name: "updated",
		last_name:  "updated2",
		phone:      "12412updated",
		email:      "updated@asd.sa",
		position:   "updated",
	}
	c.Update(contact)
	// Update(2)
	c.Delete(2)
}
